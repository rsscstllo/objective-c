# README #

### About This Repository ###

* A venue-finding app that found the user's current location, provided a list of venues (restaurants, shops, etc.) within a 10-meter radius, allowed the user to add or favorite venues, and showed directions to selected venues.  
###
* Used MapKit/CoreLocation for map functionality.  
* Used the FourSquare API for venue data.  
* Used AFNetworking for GET request to the FourSquare server.  
* Used MMRecord to combine API information with Core Data.  
* Used MMDrawerController for side-drawer (hamburger-style menu) functionality.  
###
* The app opened up to a UITableView that held all of the nearby venues.  
* Tapping into a venue would take the user to that venue on a map where he or she could get directions.  
* Opening the left side-drawer presented the user with three navigation options: Home Screen, Favorites, and Add.  
1. Home Screen: The UITableView that held all of the nearby venues.  
2. Favorites: A UITableView that held just the user's favorited venues.  
3. Add: A static page that allowed the user to manually enter a venue.  
