//
//  WTFavoriteVenuesViewController.m
//  WorldTraveler
//
//  Created by Ross Castillo on 6/25/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "WTFavoriteVenuesViewController.h"
#import "WTAppDelegate.h"
#import "Venue.h"

@interface WTFavoriteVenuesViewController ()
@property (strong, nonatomic) NSMutableArray *favorites;
@end

@implementation WTFavoriteVenuesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  self.tableView.dataSource = self;
  self.tableView.delegate  = self;
  
  if (!self.favorites)
    self.favorites = [[NSMutableArray alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
  NSPredicate *predicateForFavorites = [NSPredicate predicateWithFormat:@"favorite == %@",
                                        [NSNumber numberWithBool:YES]];
  self.favorites = [[Venue MR_findAllWithPredicate:predicateForFavorites] mutableCopy];
  [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)menuBarButtonItemPressed:(UIBarButtonItem *)sender {
  [[self drawerControllerFromAppDelegate] toggleDrawerSide:MMDrawerSideLeft
                                                  animated:YES
                                                completion:nil];
}

#pragma mark - DrawerController Helper

- (MMDrawerController *)drawerControllerFromAppDelegate {
  WTAppDelegate *appDelegate = (WTAppDelegate *)[[UIApplication sharedApplication] delegate];
  return appDelegate.drawerController;
}

#pragma mark - IBActions

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.favorites count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"
                                                          forIndexPath:indexPath];
  Venue *venue = self.favorites[indexPath.row];
  cell.textLabel.text = venue.name;
  return cell;
}

@end