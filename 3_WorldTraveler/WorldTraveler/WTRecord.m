//
//  WTRecord.m
//  WorldTraveler
//
//  Created by Ross Castillo on 6/24/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "WTRecord.h"

@implementation WTRecord

+ (NSString *)keyPathForResponseObject {
  return @"response";
}

@end