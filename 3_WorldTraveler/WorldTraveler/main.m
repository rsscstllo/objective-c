//
//  main.m
//  WorldTraveler
//
//  Created by Ross Castillo on 6/23/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WTAppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([WTAppDelegate class]));
  }
}
