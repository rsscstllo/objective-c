//
//  WTMapViewController.m
//
//
//  Created by Ross Castillo on 6/23/14.
//
//

#import "WTMapViewController.h"
#import "Location.h"
#import "FSCategory.h"
#import "WTDirectionsViewController.h"

@interface WTMapViewController ()
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *showDirectionsBarButtonItem;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@end

@implementation WTMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  self.nameLabel.text = self.venue.name;
  self.addressLabel.text = self.venue.location.address;
  
  float latitude = [self.venue.location.lat floatValue];
  float longitude = [self.venue.location.lng floatValue];
  
  CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
  MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 800, 800);
  
  [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
  
  MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
  point.coordinate = coordinate;
  point.title = self.venue.name;
  point.subtitle = self.venue.categories.name;
  
  [self.mapView addAnnotation:point];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.destinationViewController isKindOfClass:[WTDirectionsViewController class]]) {
    WTDirectionsViewController *directionsVC = segue.destinationViewController;
    directionsVC.venue = self.venue;
  }
}

#pragma mark - IBActions

- (IBAction)showDirectionsBarButtonItemPressed:(UIBarButtonItem *)sender {
  [self performSegueWithIdentifier:@"mapToDirectionsSegue" sender:nil];
}

- (IBAction)favoriteButtonPressed:(UIButton *)sender {
  self.venue.favorite = [NSNumber numberWithBool:YES];
  [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
}

@end