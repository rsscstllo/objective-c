//
//  WTViewController.m
//  WorldTraveler
//
//  Created by Ross Castillo on 6/23/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "WTListViewController.h"
#import "WTFourSquareSessionManager.h"
#import "AFMMRecordResponseSerializer.h"
#import "AFMMRecordResponseSerializationMapper.h"
#import "Venue.h"
#import "Location.h"
#import "WTMapViewController.h"
#import "WTAppDelegate.h"  // For MMDrawerController property

static NSString *const kCLIENTID = @"JF4NEFZPSNIZI4T10IULAWVSEDD4PMGJRWHM0C5K1I0UPNWW";
static NSString *const kCLIENTSECRET = @"FEYZFWUIXTM1TSBU4Y0A005GODOSWU4DUUXAUNZZV05MACIL";

#define latitudeOffset 0.01
#define longitudeOffeset 0.01

@interface WTListViewController ()<CLLocationManagerDelegate>
// iVars
@property (strong, nonatomic) NSArray *venues;
@property (strong, nonatomic) CLLocationManager *locationManager;
// IBOutlets
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *refreshBarButtonItem;
@end

@implementation WTListViewController

- (void)viewDidLoad {
  [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
  
  self.locationManager = [[CLLocationManager alloc] init];
  self.locationManager.delegate = self;
  self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
  self.locationManager.distanceFilter = 10.0;
  
  WTFourSquareSessionManager *sessionManager = [WTFourSquareSessionManager sharedClient];
  NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
  AFHTTPResponseSerializer *HTTPResponseSerializer = [AFJSONResponseSerializer serializer];
  AFMMRecordResponseSerializationMapper *mapper =
      [[AFMMRecordResponseSerializationMapper alloc] init];
  [mapper registerEntityName:@"Venue" forEndpointPathComponent:@"venues/search?"];
  AFMMRecordResponseSerializer *serializer =
      [AFMMRecordResponseSerializer serializerWithManagedObjectContext:context
                                              responseObjectSerializer:HTTPResponseSerializer
                                                          entityMapper:mapper];
  sessionManager.responseSerializer = serializer;
  
  self.tableView.dataSource = self;
  self.tableView.delegate = self;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  NSIndexPath *indexPath = sender;
  Venue *venue = self.venues[indexPath.row];
  WTMapViewController *mapVC = segue.destinationViewController;
  mapVC.venue = venue;
}

#pragma mark - IBActions

- (IBAction)refreshBarButtonItemPressed:(UIBarButtonItem *)sender {
  [self.locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
  CLLocation *location = [locations lastObject];
  [self.locationManager stopUpdatingHeading];
  [[WTFourSquareSessionManager sharedClient]
   GET:[NSString stringWithFormat:@"venues/search?ll=%f,%f",
        location.coordinate.latitude + latitudeOffset,
        location.coordinate.longitude + longitudeOffeset]
   parameters:@{@"client_id" : kCLIENTID, @"client_secret" : kCLIENTSECRET, @"v" : @"20140416"}
   success:^(NSURLSessionDataTask *task, id responseObject) {
     NSArray *venues = responseObject;
     self.venues = venues;
     [self.tableView reloadData];
   } failure:^(NSURLSessionDataTask *task, NSError *error) {
     NSLog(@"Error: %@", error);
   }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.venues count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"
                                                          forIndexPath:indexPath];
  Venue *venue = self.venues[indexPath.row];
  cell.textLabel.text = venue.name;
  cell.detailTextLabel.text = venue.location.address;
  return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [self performSegueWithIdentifier:@"listToMapSegue" sender:indexPath];
}

#pragma mark - DrawerController
// allows us to get drawer controller from app delegate
- (MMDrawerController *)drawerControllerFromAppleDelegate {
  WTAppDelegate *appDelegate = (WTAppDelegate *)[[UIApplication sharedApplication] delegate];
  return appDelegate.drawerController;
}

- (IBAction)menuBarButtonItemPressed:(UIBarButtonItem *)sender {
  [[self drawerControllerFromAppleDelegate] toggleDrawerSide:MMDrawerSideLeft
                                                    animated:YES
                                                  completion:nil];
}

@end