//
//  WTDirectionsViewController.h
//  WorldTraveler
//
//  Created by Ross Castillo on 6/25/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>
#import "Venue.h"
#import "Location.h"

@interface WTDirectionsViewController : UIViewController<MKMapViewDelegate,
                                                         CLLocationManagerDelegate>
// iVars
@property (strong, nonatomic) Venue *venue;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSArray *steps;
// IBOutlets
@property (strong, nonatomic) IBOutlet MKMapView *directionsMap;
// IBActions
- (IBAction)listDirectionsBarButtonItemPressed:(UIBarButtonItem *)sender;
@end