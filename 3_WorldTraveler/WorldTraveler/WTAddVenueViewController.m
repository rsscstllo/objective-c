//
//  WTAddVenueViewController.m
//  WorldTraveler
//
//  Created by Ross Castillo on 6/25/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "WTAddVenueViewController.h"
#import "WTAppDelegate.h"
#import "Venue.h"
#import "Contact.h"
#import "FSCategory.h"

@interface WTAddVenueViewController ()
@end

@implementation WTAddVenueViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)saveButtonPressed:(UIButton *)sender {
  if ([self.venueNameTextField.text isEqualToString:@""]) {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Blank field!"
                                                        message:@"Please enter a venue name"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
  } else {
    Venue *venue = [Venue MR_createEntity]; // create venue obj
    venue.name = self.venueNameTextField.text;
    Contact *contact = [Contact MR_createEntity];
    contact.phone = self.phoneNumberTextField.text;
    venue.contact = contact;
    
    FSCategory *category = [FSCategory MR_createEntity];
    category.name = self.typeOfFoodTextField.text;
    venue.categories = category;
    venue.favorite = [NSNumber numberWithBool:YES];
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
  }
}

- (IBAction)menuBarButtonItemPressed:(UIBarButtonItem *)sender {
  [[self drawerControllerFromAppDelegate] toggleDrawerSide:MMDrawerSideLeft
                                                  animated:YES
                                                completion:nil];
}

#pragma mark - DrawerController Helper

- (MMDrawerController *)drawerControllerFromAppDelegate {
  WTAppDelegate *appDelegate = (WTAppDelegate *)[[UIApplication sharedApplication] delegate];
  return appDelegate.drawerController;
}

@end