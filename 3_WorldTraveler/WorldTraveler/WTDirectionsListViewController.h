//
//  WTDirectionsListViewController.h
//  WorldTraveler
//
//  Created by Ross Castillo on 6/25/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WTDirectionsListViewController : UIViewController<UITableViewDataSource,
                                                             UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *steps;
@end