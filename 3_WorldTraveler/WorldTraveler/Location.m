//
//  Location.m
//  WorldTraveler
//
//  Created by Ross Castillo on 6/23/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "Location.h"

@implementation Location

@dynamic address;
@dynamic cc;
@dynamic city;
@dynamic country;
@dynamic crossStreet;
@dynamic lng;
@dynamic lat;
@dynamic postalCode;
@dynamic state;
@dynamic venue;

@end