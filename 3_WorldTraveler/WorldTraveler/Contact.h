//
//  Contact.h
//  WorldTraveler
//
//  Created by Ross Castillo on 6/23/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreData/CoreData.h>
#import "WTRecord.h"

@interface Contact : WTRecord
@property (nonatomic, retain) NSString *formattedPhone;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSManagedObject *venue;
@end