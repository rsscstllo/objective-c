//
//  WTFourSquareSessionManager.m
//  WorldTraveler
//
//  Created by Ross Castillo on 6/24/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "WTFourSquareSessionManager.h"

static NSString *const WTFourSquareBaseURLString = @"https://api.foursquare.com/v2/";

@implementation WTFourSquareSessionManager

+ (instancetype)sharedClient {
  static WTFourSquareSessionManager *_sharedClient = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _sharedClient = [[WTFourSquareSessionManager alloc]
                     initWithBaseURL:[NSURL URLWithString:WTFourSquareBaseURLString]];
  });
  return _sharedClient;
}

@end