//
//  Menu.h
//  WorldTraveler
//
//  Created by Ross Castillo on 6/23/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreData/CoreData.h>
#import "WTRecord.h"

@interface Menu : WTRecord
@property (nonatomic, retain) NSString *label;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSManagedObject *venue;
@end