//
//  WTFourSquareSessionManager.h
//  WorldTraveler
//
//  Created by Ross Castillo on 6/24/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface WTFourSquareSessionManager : AFHTTPSessionManager
+ (instancetype)sharedClient;
@end