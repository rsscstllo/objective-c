//
//  WTAddVenueViewController.h
//  WorldTraveler
//
//  Created by Ross Castillo on 6/25/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WTAddVenueViewController : UIViewController
// IBOutlets
@property (strong, nonatomic) IBOutlet UITextField *venueNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *typeOfFoodTextField;
// IBActions
- (IBAction)saveButtonPressed:(UIButton *)sender;
- (IBAction)menuBarButtonItemPressed:(UIBarButtonItem *)sender;
@end