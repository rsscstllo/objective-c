//
//  Contact.m
//  WorldTraveler
//
//  Created by Ross Castillo on 6/23/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "Contact.h"

@implementation Contact

@dynamic formattedPhone;
@dynamic phone;
@dynamic venue;

@end