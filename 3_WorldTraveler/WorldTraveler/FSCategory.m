//
//  FSCategory.m
//  WorldTraveler
//
//  Created by Ross Castillo on 6/23/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "FSCategory.h"

@implementation FSCategory

@dynamic id;
@dynamic name;
@dynamic venue;

@end