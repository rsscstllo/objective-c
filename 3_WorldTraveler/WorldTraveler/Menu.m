//
//  Menu.m
//  WorldTraveler
//
//  Created by Ross Castillo on 6/23/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "Menu.h"

@implementation Menu
@dynamic label;
@dynamic url;
@dynamic venue;
@end