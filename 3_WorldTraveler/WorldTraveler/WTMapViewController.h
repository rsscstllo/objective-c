//
//  WTMapViewController.h
//  
//
//  Created by Ross Castillo on 6/23/14.
//
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>
#import "Venue.h"

@interface WTMapViewController : UIViewController
@property (strong, nonatomic) Venue *venue;
- (IBAction)favoriteButtonPressed:(UIButton *)sender;
@end