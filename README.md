# README #

### About This Repository ###
  
* A few old Objective-C projects I worked on in my free time during the summer of 2014 after my freshman year. I spent the majority of the summer learning the fundamentals of iOS development with Objective-C from reading and watching tutorials – the three apps included were based off of tutorials followed online.  
  
* **NOTE:** These projects are out of date. They were good starting points for learning, but I no longer keep them updated as I've moved on to new projects.  
