# README #

### About This Repository ###

* A picture-taking app that allowed the user to take pictures, choose from a page of filters that loaded example thumbnails via multi-threading (GCD), and store those pictures in albums saved to Core Data.  
###
* The albums were held in a UITableView and then each album's pictures were held in a UICollectionView.  
