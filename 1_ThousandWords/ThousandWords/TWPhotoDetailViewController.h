//
//  TWPhotoDetailViewController.h
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>

// Forward declaration to merely alert this class that |class Photo| exists. This does not give
// access to the properties or public API in the |Photo| class.
@class Photo;

@interface TWPhotoDetailViewController : UIViewController
@property (strong, nonatomic) Photo *photo;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
// IBActions
- (IBAction)addFilterButtonPressed:(UIButton *)sender;
- (IBAction)deleteButtonPressed:(UIButton *)sender;
@end