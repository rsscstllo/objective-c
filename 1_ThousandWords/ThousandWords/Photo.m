//
//  Photo.m
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "Photo.h"
#import "Album.h"

@implementation Photo
@dynamic date;
@dynamic image;
@dynamic albumBook;
@end