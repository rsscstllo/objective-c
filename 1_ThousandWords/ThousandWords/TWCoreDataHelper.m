//
//  TWCoreDataHelper.m
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "TWCoreDataHelper.h"

@implementation TWCoreDataHelper

// Class method which first gets a variable that points to an instance of the AppDelegate. Using
// this instance, the NSManagedObjectContext can be retrieved for this application.
+ (NSManagedObjectContext *)managedObjectContext {
  NSManagedObjectContext *context = nil;
  id delegate = [[UIApplication sharedApplication] delegate];
  // Confirm that the delegate variable responds to the message ManagedObjectContext.
  if ([delegate performSelector:@selector(managedObjectContext)])
    context = [delegate managedObjectContext];
  return context;
}

@end