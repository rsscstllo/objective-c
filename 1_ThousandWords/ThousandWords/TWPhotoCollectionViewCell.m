//
//  TWPhotoCollectionViewCell.m
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "TWPhotoCollectionViewCell.h"

#define IMAGEVIEW_BORDER_LENGTH 5

@implementation TWPhotoCollectionViewCell

// In case a TWPhotoCell is ever created in code, make sure to call the setup method.
- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    // Initialization code
  }
  return self;
}

// Need to implement this method since this class will be created from the storyboard.
- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self)  // if this viewController exists after going through the super method.
    [self setup];
  return self;
}

// Create the UIImageView and add it to the cell's contentView in code.
- (void)setup {
  // assign value to property
  self.imageView = [[UIImageView alloc] initWithFrame:CGRectInset(self.bounds,
                                                                  IMAGEVIEW_BORDER_LENGTH,
                                                                  IMAGEVIEW_BORDER_LENGTH)];
  // ContentView is the view for the cell that we can have subviews/imageviews on top of it.
  [self.contentView addSubview:self.imageView];
}

@end