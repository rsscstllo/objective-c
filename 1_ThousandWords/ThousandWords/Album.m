//
//  Album.m
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "Album.h"
#import "Photo.h"

@implementation Album
@dynamic date;
@dynamic name;
@dynamic photos;
@end