//
//  TWAlbumTableViewController.h
//  ThousandWords
//
//  Created by Ross Castillo on 6/9/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TWAlbumTableViewController : UITableViewController<UIAlertViewDelegate>
@property (strong, nonatomic) NSMutableArray *albums;
- (IBAction)addAlbumBarButtonItemPressed:(UIBarButtonItem *)sender;
@end