//
//  TWPhotosCollectionViewController.h
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Album.h"

@interface TWPhotosCollectionViewController : UICollectionViewController
@property (strong, nonatomic) Album *album;
- (IBAction)cameraBarButtonItemPressed:(UIBarButtonItem *)sender;
@end