//
//  TWFiltersCollectionViewController.h
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Photo;  // Forward declaration.

@interface TWFiltersCollectionViewController : UICollectionViewController
@property (strong, nonatomic) Photo *photo;
@end