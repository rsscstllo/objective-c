//
//  TWCoreDataHelper.h
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TWCoreDataHelper : NSObject
// Helper method that returns an NSManagedObjectContext object from the AppDelegate.
+ (NSManagedObjectContext *)managedObjectContext;
@end