//
//  TWAlbumTableViewController.m
//  ThousandWords
//
//  Created by Ross Castillo on 6/9/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "TWAlbumTableViewController.h"
#import "Album.h"
#import "TWCoreDataHelper.h"
#import "TWPhotosCollectionViewController.h" // For segue.

@interface TWAlbumTableViewController () <UIAlertViewDelegate>
@end

@implementation TWAlbumTableViewController

#pragma mark - Lazy instantiation

- (NSMutableArray *)albums {
  if (!_albums)
    _albums = [[NSMutableArray alloc] init];
  return _albums;
}

#pragma mark - Initialization

- (id)initWithStyle:(UITableViewStyle)style {
  self = [super initWithStyle:style];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Uncomment the following line to preserve selection between presentations.
  // self.clearsSelectionOnViewWillAppear = NO;
  
  // Uncomment the following line to display an Edit button in the navigation bar for this vc.
  // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

// Each time the view appears on the screen do a query to core data for all Album objects that have
// been saved.
- (void)viewWillAppear:(BOOL)animated {
  // Do the functionality in the super class.
  [super viewWillAppear:animated];
  
  // Used as a search criteria for NSManagedObjects in database.
  NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Album"];
  fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES]];
  NSError *error = nil;
  NSArray *fetchedAlbums = [[TWCoreDataHelper managedObjectContext] executeFetchRequest:fetchRequest
                                                                                  error:&error];
  self.albums = [fetchedAlbums mutableCopy];
  [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:@"Album Chosen"]) {
    if ([segue.destinationViewController isKindOfClass:[TWPhotosCollectionViewController class]]) {
      NSIndexPath *path = [self.tableView indexPathForSelectedRow];
      TWPhotosCollectionViewController *targetViewController = segue.destinationViewController;
      targetViewController.album = self.albums[path.row];
    }
  }
}

#pragma mark - IBActions

// Create a UIAlertView Object with a button to add albums.
- (IBAction)addAlbumBarButtonItemPressed:(UIBarButtonItem *)sender {
  // Make sure to set the delegate property to self this time.
  UIAlertView *newAlbumAlertView = [[UIAlertView alloc] initWithTitle:@"Enter New Album Name"
                                                              message:nil delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                                    otherButtonTitles:@"Add", nil];
  [newAlbumAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
  [newAlbumAlertView show];
}

#pragma mark - UIAlertViewDelegate

// Checking to see if the buttonIndex is 1, the location of the Add button. Then grab the text from
// the textfield, using a method |textFieldAtIndex| implemented by UIAlertView.
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (buttonIndex == 1) {
    NSString *alertText = [alertView textFieldAtIndex:0].text;
    [self.albums addObject:[self albumWithName:alertText]];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[self.albums count] - 1
                                                                inSection:0]]
                          withRowAnimation:UITableViewRowAnimationFade];
  }
}

#pragma mark - TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.albums count];
}

// Use the Album object to display information in the tableView cell.
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *CellIdentifier = @"Cell";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier
                                                          forIndexPath:indexPath];
  // Configure the cell...
  Album *selectedAlbum = self.albums[indexPath.row];
  cell.textLabel.text = selectedAlbum.name;
  return cell;
}

#pragma mark - Helper Methods

// Persisting an Album object using Core Data to our file system.
- (Album *)albumWithName:(NSString *)name {
  NSManagedObjectContext *context = [TWCoreDataHelper managedObjectContext];
  // Album is a subclass of NSManagedObject.
  // Create new persistent album obj.
  // NSEntityEescription is required to save objects to CoreData.
  Album *album = [NSEntityDescription insertNewObjectForEntityForName:@"Album"
                                               inManagedObjectContext:context];
  album.name = name;
  album.date = [NSDate date];
  NSError *error = nil;
  if (![context save:&error]) {  // |&| is a pointer to a pointer.
    // error
  }
  return album;
}

@end