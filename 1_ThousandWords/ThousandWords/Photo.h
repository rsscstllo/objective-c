//
//  Photo.h
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Album;

// Generated from xcdatamodeled.
@interface Photo : NSManagedObject
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) id image;
@property (nonatomic, retain) Album *albumBook;
@end