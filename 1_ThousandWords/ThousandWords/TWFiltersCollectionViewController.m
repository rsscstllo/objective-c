//
//  TWFiltersCollectionViewController.m
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "TWFiltersCollectionViewController.h"
#import "TWPhotoCollectionViewCell.h"
#import "Photo.h"

@interface TWFiltersCollectionViewController ()
@property (strong, nonatomic) NSMutableArray *filters;
@property (strong, nonatomic) CIContext *context;
@end

@implementation TWFiltersCollectionViewController

#pragma mark - Lazy Instantiation

- (NSMutableArray *)filters {
  if (!_filters) _filters = [[NSMutableArray alloc] init];
  return _filters;
}

- (CIContext *)context {
  if (!_context) _context = [CIContext contextWithOptions:nil];
  return _context;
}

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  self.filters = [[[self class] photoFilters] mutableCopy];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView DataSource

// This method sets up the CollectionView cells with the UIImages that have the filters applied.
// A queue is created that pushes the |filteredImageFromImage| intensive method onto another thread
// to save time on main thread while the filtered images update. So, as the images are returned,
// they'll be updated to the UI asynchronously.
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  
  static NSString *CellIdentifier = @"Photo Cell";
  TWPhotoCollectionViewCell *cell =
  [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
  
  cell.backgroundColor = [UIColor whiteColor];
  
  dispatch_queue_t filterQueue = dispatch_queue_create("filter queue", NULL); // Creating the queue
  
  dispatch_async(filterQueue, ^{
    UIImage *filterImage = [self filteredImageFromImage:self.photo.image
                                              andFilter:self.filters[indexPath.row]];
    dispatch_async(dispatch_get_main_queue(), ^{
      cell.imageView.image = filterImage;
    });
  });
  return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return [self.filters count];
}

#pragma mark - UICollectionView Delegate

// When the user selects one of the filters save the image with the filter.
- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  // Casting item to be of type TWPhotoCollectionViewCell.
  TWPhotoCollectionViewCell *selectedCell =
  (TWPhotoCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
  self.photo.image = selectedCell.imageView.image;
  if (self.photo.image) {
    NSError *error = nil;
    if (![[self.photo managedObjectContext] save:&error]) {
      // Handle the error.
    }
    [self.navigationController popToRootViewControllerAnimated:YES ];
  }
}

#pragma mark - Helper Methods

// Set up all the apple defined filters for this application return them in an array.
+ (NSArray *)photoFilters {
  CIFilter *sepia = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:nil];
  CIFilter *blur = [CIFilter filterWithName:@"CIGaussianBlur" keysAndValues:nil];
  CIFilter *instant = [CIFilter filterWithName:@"CIPhotoEffectInstant" keysAndValues:nil];
  CIFilter *noir = [CIFilter filterWithName:@"CIPhotoEffectNoir" keysAndValues: nil];
  CIFilter *vignette = [CIFilter filterWithName:@"CIVignetteEffect" keysAndValues: nil];
  CIFilter *transfer = [CIFilter filterWithName:@"CIPhotoEffectTransfer" keysAndValues: nil];
  CIFilter *unsharpen = [CIFilter filterWithName:@"CIUnsharpMask" keysAndValues: nil];
  CIFilter *monochrome = [CIFilter filterWithName:@"CIColorMonochrome" keysAndValues: nil];
  CIFilter *colorControls = [CIFilter filterWithName:@"CIColorControls"
                                       keysAndValues:kCIInputSaturationKey, @0.5, nil];
  CIFilter *colorClamp = [CIFilter filterWithName:@"CIColorClamp"
                                    keysAndValues:@"inputMaxComponents",
                          [CIVector vectorWithX:0.9 Y:0.9 Z:0.9 W:0.9],
                          @"inputMinComponents",
                          [CIVector vectorWithX:0.2 Y:0.2 Z:0.2 W:0.2],nil];
  // Add every filter to an array.
  NSArray *allFilters = @[sepia, blur, colorClamp, instant, noir, vignette, colorControls, transfer,
                          unsharpen, monochrome];
  return allFilters;
}

// This method returns a UIImage with a filter applied. In order for the filters to be applied,
// conversions must be made - For example, a UIImage must be converted into a CIImage, which can
// have filters applied to it.
- (UIImage *)filteredImageFromImage:(UIImage *)image andFilter:(CIFilter *)filter {
  // Get a CIImage.
  CIImage *unfilteredImage = [[CIImage alloc] initWithCGImage:image.CGImage];
  // Set up filter with apple defined key.
  [filter setValue:unfilteredImage forKey:kCIInputImageKey];
  // Get the filtered image back.
  CIImage *filteredImage = [filter outputImage];
  // Figure out how big image should be.
  CGRect extent = [filteredImage extent];
  // Convert CIImage into a CGImage ref in order to convert it to a UIImage.
  CGImageRef cgImage = [self.context createCGImage:filteredImage fromRect:extent];
  UIImage *finalImage = [UIImage imageWithCGImage:cgImage];
  CGImageRelease(cgImage);
  return finalImage;
}

@end