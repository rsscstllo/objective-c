//
//  TWPhotosCollectionViewController.m
//  ThousandWords
//
//  Created by Ross Castillo on 6/10/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "TWPhotosCollectionViewController.h"
#import "TWPhotoCollectionViewCell.h"
#import "Photo.h"
#import "TWPictureDataTransformer.h"
#import "TWCoreDataHelper.h"
#import "TWPhotoDetailViewController.h"

@interface TWPhotosCollectionViewController () <UIImagePickerControllerDelegate,
UINavigationControllerDelegate>
// NSMutableArray of UIImages
@property (strong, nonatomic) NSMutableArray *photos;
@end

@implementation TWPhotosCollectionViewController

#pragma mark - Lazy Instantiation
- (NSMutableArray *)photos {
  if (!_photos)
    _photos = [[NSMutableArray alloc] init];
  return _photos;
}
#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
}

// Access the photos from Core Data in the viewWillAppear method since this method is called
// everytime this viewcontroller appears on the screen instead of only the first time (viewDidLoad
// only is called only when it is created).
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:YES];
  // The Photos are stored in Core Data as an NSSet. An NSSet is an unorderd collection where an
  // object cannot be repeated.
  NSSet *unorderdPhotos = self.album.photos;
  // To organize them, an NSSort descriptor arranges the photos by date.
  NSSortDescriptor *dateDescripter = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
  // Convert set to array.
  NSArray *sortedPhotos = [unorderdPhotos sortedArrayUsingDescriptors:@[dateDescripter]];
  self.photos = [sortedPhotos mutableCopy];
  // Now that the photos are arranged the CollectionView can be reloaded.
  [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  // Confirm that the correct segue is being triggered.
  if ([segue.identifier isEqualToString:@"Detail Segue"]) {
    // Confirm that the correct View Controller is being transitioned to.
    if ([segue.destinationViewController isKindOfClass:[TWPhotoDetailViewController class]]) {
      // Create an object of that class/viewController.
      TWPhotoDetailViewController *targetViewController = segue.destinationViewController;
      // Which cell was pressed - last object of array, or last cell that was selected.
      NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] lastObject];
      // Access the photo that was tapped and set the property of the target ViewController.
      Photo *selectedPhoto = self.photos[indexPath.row];
      targetViewController.photo = selectedPhoto;
    }
  }
}

#pragma mark - IBActions

// When the camera button is pressed, create a UIImagePickerController and present the avaliable
// option on the screen. If the camera is avaliable because the user is using an actual phone, show
// that. If not, then show the Photos Album.
- (IBAction)cameraBarButtonItemPressed:(UIBarButtonItem *)sender {
  // Create a UIImagePicker object and set its' delegate property to self so its ImagePicker
  // delegate methods can be implemented. This allows for blind communication between
  // UIImagePickerController and collectionViewController.
  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
  picker.delegate = self;
  // If the Camera is avaliable use it to choose the image.
  if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
  }
  // Otherwise, use the iPhone photo album.
  else if ([UIImagePickerController
            isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
  }
  [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource

// Display the photos stored in the |photos| array.
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *CellIdentifier = @"Photo Cell";
  TWPhotoCollectionViewCell *cell =
      [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
  // Access the correct photo from the photo's array.
  Photo *photo = self.photos[indexPath.row];
  cell.backgroundColor = [UIColor whiteColor];
  cell.imageView.image = photo.image;
  return cell;
}

// Number of items in the collection view should be equal to the number of photos in the photos
// array.
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return [self.photos count];
}

#pragma mark - UIImagePickerControllerDelegate

// If the user picks chooses media this delegate method will fire. The parameters can be used to
// determine which media object the user selected.
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
  UIImage *image = info[UIImagePickerControllerEditedImage];
  if (!image) image = info[UIImagePickerControllerOriginalImage];
  // Add the image object to a new array.
  [self.photos addObject:[self photoFromImage:image]];
  // Reload collectionView to display images that were just grabbed.
  [self.collectionView reloadData];
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Helper methods

// Instance method which accepts a UIImage and persists it to Core Data.
- (Photo *)photoFromImage:(UIImage *)image {
  Photo *photo =
  [NSEntityDescription insertNewObjectForEntityForName:@"Photo"
                                inManagedObjectContext:[TWCoreDataHelper managedObjectContext]];
  photo.image = image;
  photo.date = [NSDate date];
  photo.albumBook = self.album;
  NSError *error = nil;
  if (![[photo managedObjectContext] save:&error]) {
    // Error message in saving.
    NSLog(@"%@", error);
  }
  return photo;
}

@end