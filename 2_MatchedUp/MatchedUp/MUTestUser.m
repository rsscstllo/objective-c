//
//  MUTestUser.m
//  MatchedUp
//
//  Created by Ross Castillo on 6/20/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "MUTestUser.h"

@implementation MUTestUser

+ (void)saveTestUserToParse {
  // Create a new user.
  PFUser *newUser = [PFUser user];
  newUser.username = @"user1";
  newUser.password = @"password1";
  [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
    if (!error) {
      NSDictionary *profile = @{@"age" : @22,
                                @"birthday"   : @"11/22/1985",
                                @"firstName"  : @"Julie",
                                @"gender"     : @"female",
                                @"location"   : @"Berlin, Germany",
                                @"name"       : @"Julie Adams"
                               };
      [newUser setObject:profile forKey:@"profile"];
      [newUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        UIImage *profileImage = [UIImage imageNamed:@"JuliaProfilePicture.jpg"];
        // Create a data representation of this image.
        NSData *imageData = UIImageJPEGRepresentation(profileImage, 0.8);
        // Create a PFFile from the NSData just created above.
        PFFile *photoFile = [PFFile fileWithData:imageData];
        [photoFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
          if (succeeded) {  // If in fact our PFFile was uploaded
            PFObject *photo = [PFObject objectWithClassName:kMUPhotoClassKey];
            [photo setObject:newUser forKey:KMUPhotoUserKey];
            [photo setObject:photoFile forKey:kMUPhotoPictureKey];
            [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
              NSLog(@"photo saved successfully");
            }];
          }
        }];
      }];
    }
  }];
}

@end
