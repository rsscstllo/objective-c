//
//  MUMatchViewController.m
//  MatchedUp
//
//  Created by Ross Castillo on 6/20/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "MUMatchViewController.h"

@interface MUMatchViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *matchedUserImageView;
@property (strong, nonatomic) IBOutlet UIImageView *currentUserImageView;
@property (strong, nonatomic) IBOutlet UIButton *viewChatsButton;
@property (strong, nonatomic) IBOutlet UIButton *keepSearchingButton;
@end

@implementation MUMatchViewController

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  PFQuery *query = [PFQuery queryWithClassName:kMUPhotoClassKey];
  // Give back only the photo for the current user.
  [query whereKey:KMUPhotoUserKey equalTo:[PFUser currentUser]];
  // Run the query asynchronsly on a background thread.
  [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
    if ([objects count] > 0) {  // Only allow user to get one picture.
      PFObject *photo = objects[0];
      PFFile *pictureFile = photo[kMUPhotoPictureKey];  // Where our data is stored.
      [pictureFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        self.currentUserImageView.image = [UIImage imageWithData:data];
        self.matchedUserImageView.image = self.matchedUserImage;
      }];
    }
  }];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)viewChatsButtonPressed:(UIButton *)sender {
  [self.delegate presentMatchesViewController];
}

- (IBAction)keepSearchingButtonPressed:(UIButton *)sender {
  // Modal segue, so view controller is dismissed rather than popped off.
  [self dismissViewControllerAnimated:YES completion:nil];
}

@end