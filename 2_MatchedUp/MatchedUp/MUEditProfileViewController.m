//
//  MUEditProfileViewController.m
//  MatchedUp
//
//  Created by Ross Castillo on 6/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "MUEditProfileViewController.h"

@interface MUEditProfileViewController ()
@property (strong, nonatomic) IBOutlet UITextView *tagLineTextView;
@property (strong, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveBarButtonItem;
@end

@implementation MUEditProfileViewController

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  self.view.backgroundColor = [UIColor colorWithRed:242/255.0
                                              green:242/255.0
                                               blue:242/255.0
                                              alpha:1.0];
  PFQuery *query = [PFQuery queryWithClassName:kMUPhotoClassKey];
  [query whereKey:KMUPhotoUserKey equalTo:[PFUser currentUser]];
  [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
    if ([objects count] > 0) {
      PFObject *photo = objects[0];
      PFFile *pictureFile = photo[kMUPhotoPictureKey];
      [pictureFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        self.profilePictureImageView.image = [UIImage imageWithData:data];
      }];
    }
  }];
  self.tagLineTextView.text = [[PFUser currentUser] objectForKey:kMUUserTagLineKey];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

// Save info and return back to the previous viewController.
- (IBAction)saveBarButtonItemPressed:(UIBarButtonItem *)sender {
  [[PFUser currentUser] setObject:self.tagLineTextView.text forKey:kMUUserTagLineKey];
  [[PFUser currentUser] saveInBackground];
  [self.navigationController popViewControllerAnimated:YES];
}

@end