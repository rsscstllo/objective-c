//
//  MUChatViewController.h
//  MatchedUp
//
//  Created by Ross Castillo on 6/20/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "JSMessagesViewController.h"

@interface MUChatViewController : JSMessagesViewController<JSMessagesViewDataSource,
                                                           JSMessagesViewDelegate>
@property (strong, nonatomic) PFObject *chatRoom;
@end