//
//  MUConstants.h
//  MatchedUp
//
//  Created by Ross Castillo on 6/14/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MUConstants : NSObject
#pragma mark - User Class
// Declaring an immutable pointer to a mutable NSString object.
// This pointer will exist in memory during the life of the program.
// The value of the NSString can be changed despite it being a const pointer.
extern NSString *const kMUUserProfileKey;  // Access to entire profile
extern NSString *const kMUUserProfileNameKey;
extern NSString *const kMUUserProfileFirstNameKey;
extern NSString *const kMUUserProfileLocationKey;
extern NSString *const kMUUserProfileGenderKey;
extern NSString *const kMUUserProfileBirthdayKey;
extern NSString *const kMUUserProfileInterestedInKey;
extern NSString *const kMUUserProfilePictureURL;
extern NSString *const kMUUserProfileRelationshipStatusKey;
extern NSString *const kMUUserprofileAgeKey;

extern NSString *const kMUUserTagLineKey;  // Not a part of the profile, but saved to the user.

#pragma mark - Photo Class
extern NSString *const kMUPhotoClassKey;
extern NSString *const KMUPhotoUserKey;
extern NSString *const kMUPhotoPictureKey;

#pragma mark - Activity Class
extern NSString *const kMUActivityClassKey;
extern NSString *const kMUActivityTypeKey;
extern NSString *const kMUActivityFromUserKey;
extern NSString *const kMUActivityToUserKey;
extern NSString *const kMUActivityPhotoKey;
extern NSString *const kMUActivityTypeLikeKey;
extern NSString *const kMUactivityTypeDislikeKey;

#pragma mark - Settings
extern NSString *const kMUMenEnabledKey;
extern NSString *const kMUWomenEnabledKey;
extern NSString *const kMUSingleEnabledKey;
extern NSString *const kMUAgeMaxKey;

#pragma mark - ChatRoom
extern NSString *const kMUChatRoomClassKey;
extern NSString *const kMUChatRoomUser1Key;
extern NSString *const kMUChatRoomUser2Key;

#pragma mark - Chat
extern NSString *const kMUChatClassKey;
extern NSString *const kMUChatChatroomKey;
extern NSString *const kMUChatFromUserKey;
extern NSString *const kMUChatToUserKey;
extern NSString *const kMUChatTextKey;
@end