//
//  MUTestUser2.h
//  MatchedUp
//
//  Created by Ross Castillo on 6/21/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MUTestUser2 : NSObject
+ (void)saveTestUserToParse;
@end