//
//  MUHomeViewController.m
//  MatchedUp
//
//  Created by Ross Castillo on 6/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "MUHomeViewController.h"
#import "MUTestUser.h"
#import "MUTestUser2.h"
#import "MUProfileViewController.h"
#import "MUMatchViewController.h"
#import "MUTransitionAnimator.h"

@interface MUHomeViewController () <MUMatchViewControllerDelegate, MUProfileViewControllerDelegate,
                                    UIViewControllerTransitioningDelegate>
// IBOutlets
@property (strong, nonatomic) IBOutlet UIBarButtonItem *chatBarButtonItem;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *settingsBarButtonItem;
@property (strong, nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *ageLabel;
// @property (strong, nonatomic) IBOutlet UILabel *tagLineLabel;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *infoButton;
@property (strong, nonatomic) IBOutlet UIButton *dislikeButton;
@property (strong, nonatomic) IBOutlet UIView *labelContainerView;
@property (strong, nonatomic) IBOutlet UIView *buttonContainerView;

// Instance Variables
// Used to store photos from parse.
@property (strong, nonatomic) NSArray *photos;
// Keep track of current photo we're viewing in the application.
@property (strong, nonatomic) PFObject *photo;
// Keep track of who we've liked and which user that belongs to - who did liking and who was liked.
@property (strong, nonatomic) NSMutableArray *activities;
// Keep track of photo we're viewing in the array.
@property (nonatomic) int currentPhotoIndex;
// Keep track of if, for the current photo being displayed, I have already liked/disliked this user?
@property (nonatomic) BOOL isLikedByCurrentUser;
@property (nonatomic) BOOL isDislikedByCurrentUser;
@end

@implementation MUHomeViewController

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  // Only call this once so multiple users with same user name aren't made.
  // [MUTestUser saveTestUserToParse];
  // [MUTestUser2 saveTestUserToParse];
  [self setUpViews];
}

- (void)viewDidAppear:(BOOL)animated {
  self.photoImageView.image = nil;
  self.firstNameLabel.text = nil;
  self.ageLabel.text = nil;
  // Don't want our user to be able to immediately press the like/dislike buttons.
  self.likeButton.enabled = NO;
  self.dislikeButton.enabled = NO;
  self.infoButton.enabled = NO;
  // Set up the current index to start off with the first photo we get back from parse.
  self.currentPhotoIndex = 0;
  // Get back the objects that were saved to the Photo class.
  PFQuery *query = [PFQuery queryWithClassName:kMUPhotoClassKey];
  [query whereKey:KMUPhotoUserKey notEqualTo:[PFUser currentUser]];
  // When dealing with related objects, with parse, our photo has a property |user|.
  // |includeKey| says that when we download the photo, we should also download the user as well.
  // Useful for when we have many photos with a lot of different users.
  [query includeKey:KMUPhotoUserKey];
  // Kicking off a backgroud thread to evaluate the code in this block while this program continues
  // with the code following the block.
  [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
    if (!error) {
      self.photos = objects;  // Store the array we get back in |self.photos| so we can access them.
      if ([self allowPhoto] == NO) {
        [self setUpNextPhoto];
      } else {
        // After we update |self.photos| to equal the |objects| we got back from parse.
        [self queryForCurrentPhotoIndex];
      }
    } else
      NSLog(@"Error");
  }];
}

#pragma mark - Initialization Helper Methods

- (void)setUpViews {
  self.view.backgroundColor = [UIColor colorWithRed:242/255.0
                                              green:242/255.0
                                               blue:242/255.0
                                              alpha:1.0];
  [self addShadowForView:self.buttonContainerView];
  [self addShadowForView:self.labelContainerView];
  self.photoImageView.layer.masksToBounds = YES;
}

- (void)addShadowForView:(UIView *)view {
  view.layer.masksToBounds = NO;  // Any subview will be clipped.
  view.layer.cornerRadius = 4;    // Rounded Corners (4 is high).
  view.layer.shadowRadius = 1;
  view.layer.shadowOffset = CGSizeMake(0, 1);
  view.layer.shadowOpacity = 0.25;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:@"homeToProfileSegue"]) {
    MUProfileViewController *profileVC = segue.destinationViewController;
    profileVC.photo = self.photo;
    profileVC.delegate = self;
  } else if ([segue.identifier isEqualToString:@"homeToMatchSegue"]) {
    // Pass the photo from this view controller to our match veiw controller.
    MUMatchViewController *matchVC = segue.destinationViewController;
    matchVC.matchedUserImage = self.photoImageView.image;
    matchVC.delegate = self;
  }
}

#pragma mark - IBActions

- (IBAction)settingsBarButtonItemPressed:(UIBarButtonItem *)sender {
  // ...
}

- (IBAction)chatBarButtonItemPressed:(UIBarButtonItem *)sender {
  [self performSegueWithIdentifier:@"homeToMatchesSegue" sender:nil];
}

- (IBAction)likeButtonPressed:(UIButton *)sender {
  [self checkLike];
}

- (IBAction)dislikeButtonPressed:(UIButton *)sender {
  [self checkDislike];
}

- (IBAction)infoButtonPressed:(UIButton *)sender {
  [self performSegueWithIdentifier:@"homeToProfileSegue" sender:nil];
}

#pragma mark - Helper Methods

// Query for the current photo index to get back a PFFile which will store the data for the image
// that we want to display for our user.
- (void)queryForCurrentPhotoIndex {
  if ([self.photos count] > 0) {  // Make sure we even have an array of photos.
    self.photo = self.photos[self.currentPhotoIndex];
    // Since we have this photo object we can now access the value for the key image for a PFFile.
    PFFile *file = self.photo[kMUPhotoPictureKey];
    // Now, actually download the file.
    [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
      if (!error) {
        UIImage *image = [UIImage imageWithData:data];
        self.photoImageView.image = image;  // Update with downloaded image.
        [self updateView];
      } else
        NSLog(@"%@", error);
    }];
    // When the viewController is loaded up, we should search for any likes/dislikes that exist.
    PFQuery *queryForLike = [PFQuery queryWithClassName:kMUActivityClassKey];
    [queryForLike whereKey:kMUActivityTypeKey equalTo:kMUActivityTypeLikeKey];
    [queryForLike whereKey:kMUActivityPhotoKey equalTo:self.photo];  // Get like for this photo.
    // Get likes related to the current user and not all likes on this photo
    [queryForLike whereKey:kMUActivityFromUserKey equalTo:[PFUser currentUser]];
    // Dislikes.
    PFQuery *queryForDislike = [PFQuery queryWithClassName:kMUActivityClassKey];
    [queryForDislike whereKey:kMUActivityTypeKey equalTo:kMUactivityTypeDislikeKey];
    [queryForDislike whereKey:kMUActivityPhotoKey equalTo:self.photo];
    [queryForDislike whereKey:kMUActivityFromUserKey equalTo:[PFUser currentUser]];
    // Join the two querys together.
    PFQuery *likeAndDislikeQuery = [PFQuery orQueryWithSubqueries:@[queryForLike, queryForDislike]];
    [likeAndDislikeQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
      if (!error) {
        self.activities = [objects mutableCopy];
        if ([self.activities count] == 0) {
          self.isLikedByCurrentUser = NO;
          self.isDislikedByCurrentUser = NO;
        } else {  // If the array 'does' have a like or dislike inside of it.
          PFObject *activity = self.activities[0];
          if ([activity[kMUActivityTypeKey] isEqualToString:kMUActivityTypeLikeKey]) {
            self.isLikedByCurrentUser = YES;
            self.isDislikedByCurrentUser = NO;
          } else if ([activity[kMUActivityTypeKey] isEqualToString:kMUactivityTypeDislikeKey]) {
            self.isLikedByCurrentUser = NO;
            self.isDislikedByCurrentUser = YES;
          } else {
            // Some other type of activity...
          }
        }
        // Enable buttons after the query inside this block
        self.likeButton.enabled = YES;
        self.dislikeButton.enabled = YES;
        self.infoButton.enabled = YES;
      }
    }];
  }
}

// For this photo, get back the user, access its profile, and access first name.
- (void)updateView {
  self.firstNameLabel.text =
      self.photo[KMUPhotoUserKey][kMUUserProfileKey][kMUUserProfileFirstNameKey];
  self.ageLabel.text = [NSString stringWithFormat:@"%@",
                        self.photo[KMUPhotoUserKey][kMUUserProfileKey][kMUUserprofileAgeKey]];
  // Display the tag line.
  // self.tagLineLabel.text = self.photo[KMUPhotoUserKey][kMUUserTagLineKey];
}

// If the user wants to see the next photo after pressing the like or dislike button.
- (void)setUpNextPhoto {
  if (self.currentPhotoIndex + 1 < self.photos.count) {  // See if the next index even exists.
    self.currentPhotoIndex++;  // Add one to the current index.
    if ([self allowPhoto] == NO) {
      [self setUpNextPhoto];
    } else {
      [self queryForCurrentPhotoIndex];
    }
  } else {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No more users to view"
                                                    message:@"Check back later for more people"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
  }
}

- (void)saveLike {
  // Upper-case 'A' for |Activity| since we're creating a new class.
  PFObject *likeActivity = [PFObject objectWithClassName:kMUActivityClassKey];
  // Storing that it is infact a 'like' for the key type.
  [likeActivity setObject:kMUActivityTypeLikeKey forKey:kMUActivityTypeKey];
  // Record that current user logged in was the one who did the liking.
  [likeActivity setObject:[PFUser currentUser] forKey:kMUActivityFromUserKey];
  // Find out which user owns this photo and then save the user that we liked.
  [likeActivity setObject:[self.photo objectForKey:KMUPhotoUserKey] forKey:kMUActivityToUserKey];
  // Also, save the photo that was involved with this liking.
  [likeActivity setObject:self.photo forKey:kMUActivityPhotoKey];
  // Save this like in the background to parse.
  [likeActivity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
    // Add some code after the saving is done.
    [self checkForPhotoUserLikes];
    self.isLikedByCurrentUser = YES;
    self.isDislikedByCurrentUser = NO;
    [self.activities addObject:likeActivity];  // Add to our activities array.
    [self setUpNextPhoto];
  }];
}

// Could possibly integrate this into the |saveLike| method with a paramter to make things more
// dynamic.
- (void)saveDislike {
  PFObject *dislikeActivity = [PFObject objectWithClassName:kMUActivityClassKey];
  [dislikeActivity setObject:kMUactivityTypeDislikeKey forKey:kMUActivityTypeKey];
  [dislikeActivity setObject:[PFUser currentUser] forKey:kMUActivityFromUserKey];
  [dislikeActivity setObject:[self.photo objectForKey:KMUPhotoUserKey] forKey:kMUActivityToUserKey];
  [dislikeActivity setObject:self.photo forKey:kMUActivityPhotoKey];
  [dislikeActivity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
    self.isLikedByCurrentUser = NO;
    self.isDislikedByCurrentUser = YES;
    [self.activities addObject:dislikeActivity];
    [self setUpNextPhoto];
  }];
}

- (void)checkLike {
  if (self.isLikedByCurrentUser) {  // If we've liked this current user.
    [self setUpNextPhoto];
    return;
  } else if (self.isDislikedByCurrentUser) {  // If we've disliked this current user.
    for (PFObject *activity in self.activities)
      [activity deleteInBackground];     // Remove from parse.
    [self.activities removeLastObject];  // Remove the activity from our array of activites.
    [self saveLike];
  } else  // If we haven't liked or disliked the photo, then like it.
    [self saveLike];
}

- (void)checkDislike {
  if (self.isDislikedByCurrentUser) {  // If we've disliked this current user.
    [self setUpNextPhoto];
    return;
  } else if (self.isLikedByCurrentUser) {  // If we've liked this current user.
    for (PFObject *activity in self.activities)
      [activity deleteInBackground];
    [self.activities removeLastObject];
    [self saveDislike];
  } else  // If we haven't liked or disliked the photo, then dislike it.
    [self saveDislike];
}

- (void)checkForPhotoUserLikes {  // or/a.k.a, |checkForChatRoom| method.
  PFQuery *query = [PFQuery queryWithClassName:kMUActivityClassKey];
  [query whereKey:kMUActivityFromUserKey equalTo:self.photo[KMUPhotoUserKey]];
  [query whereKey:kMUActivityToUserKey equalTo:[PFUser currentUser]];
  [query whereKey:kMUActivityTypeKey equalTo:kMUActivityTypeLikeKey];
  [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
    if (objects.count > 0) // If there's indeed an array, then create the chatroom.
      [self createChatRoom];
  }];
}

- (void)createChatRoom {
  PFQuery *queryForChatRoom = [PFQuery queryWithClassName:kMUChatRoomClassKey];
  // Give me back all the chat rooms where user1 is equal to the current user.
  [queryForChatRoom whereKey:kMUChatRoomUser1Key equalTo:[PFUser currentUser]];
  // User2 should be equal to the photo we're currently viewing in our home viewController.
  [queryForChatRoom whereKey:kMUChatRoomUser2Key equalTo:self.photo[KMUPhotoUserKey]];
  PFQuery *queryForChatRoomInverse = [PFQuery queryWithClassName:kMUChatRoomClassKey];
  [queryForChatRoomInverse whereKey:kMUChatRoomUser1Key equalTo:self.photo[KMUPhotoUserKey]];
  [queryForChatRoomInverse whereKey:kMUChatRoomUser2Key equalTo:[PFUser currentUser]];
  // Combine both queries so we only have to call |findObjectsInBackgroundWithBlock| once.
  PFQuery *combineQuery = [PFQuery orQueryWithSubqueries:@[queryForChatRoom,
                                                           queryForChatRoomInverse]];
  [combineQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
    if (error)
      NSLog(@"%@", error);
    else if (objects.count == 0) {  // No objects were returned, so need to creat a chatroom object.
      PFObject *chatRoom = [PFObject objectWithClassName:kMUChatRoomClassKey];
      [chatRoom setObject:[PFUser currentUser] forKey:kMUChatRoomUser1Key];
      [chatRoom setObject:self.photo[KMUPhotoUserKey] forKey:kMUChatRoomUser2Key];
      [chatRoom saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        // [self performSegueWithIdentifier:@"homeToMatchSegue" sender:nil];
        // This instance of view controller knows how to access storyboard
        UIStoryboard *myStoryboard = self.storyboard;
        MUMatchViewController *matchViewController =
        [myStoryboard instantiateViewControllerWithIdentifier:@"matchVC"];
        matchViewController.view.backgroundColor = [UIColor colorWithRed:0/255.0
                                                                   green:0/255.0
                                                                    blue:0/255.0
                                                                   alpha:.75];
        matchViewController.transitioningDelegate = self;
        matchViewController.matchedUserImage = self.photoImageView.image;
        matchViewController.delegate = self;
        matchViewController.modalPresentationStyle = UIModalPresentationCustom;
        [self presentViewController:matchViewController animated:YES completion:nil];
      }];
    }
  }];
}

- (BOOL)allowPhoto {
  int maxAge = (int)[[NSUserDefaults standardUserDefaults] integerForKey:kMUAgeMaxKey];
  BOOL men = [[NSUserDefaults standardUserDefaults] boolForKey:kMUMenEnabledKey];
  BOOL women = [[NSUserDefaults standardUserDefaults] boolForKey:kMUWomenEnabledKey];
  BOOL single = [[NSUserDefaults standardUserDefaults] boolForKey:kMUSingleEnabledKey];
  // Get back info from our current user for comparison.
  PFObject *photo = self.photos[self.currentPhotoIndex];
  PFUser *user = photo[KMUPhotoUserKey];
  int userAge = [user[kMUUserProfileKey][kMUUserprofileAgeKey] intValue];
  NSString *gender = user[kMUUserProfileKey][kMUUserProfileGenderKey];
  NSString *relationshipStatus = user[kMUUserProfileKey][kMUUserProfileRelationshipStatusKey];
  if (userAge > maxAge) return NO;
  else if (men == NO && [gender isEqualToString:@"male"]) return NO;
  else if (women == NO && [gender isEqualToString:@"female"]) return NO;
  else if (single == NO &&
           ([relationshipStatus isEqualToString:@"single"] || relationshipStatus == nil)) return NO;
  else return YES;
}

#pragma mark - MUMatchViewController Delegate

- (void)presentMatchesViewController {
  [self dismissViewControllerAnimated:NO completion:^{
    [self performSegueWithIdentifier:@"homeToMatchesSegue" sender:nil];
  }];
}

#pragma mark - MUProfileViewController Delegate

- (void)didPressLike {
  [self.navigationController popViewControllerAnimated:NO];
  [self checkLike];
}

- (void)didPressDislike {
  [self.navigationController popViewControllerAnimated:NO];
  [self checkDislike];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)
    animationControllerForPresentedController:(UIViewController *)presented
                         presentingController:(UIViewController *)presenting
                             sourceController:(UIViewController *)source {
  MUTransitionAnimator *animator = [[MUTransitionAnimator alloc] init];
  animator.presenting = YES;
  return animator;
}

- (id<UIViewControllerAnimatedTransitioning>)
    animationControllerForDismissedController:(UIViewController *)dismissed {
  MUTransitionAnimator *animator = [[MUTransitionAnimator alloc] init];
  return animator;
}

@end