//
//  MUMatchViewController.h
//  MatchedUp
//
//  Created by Ross Castillo on 6/20/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MUMatchViewControllerDelegate<NSObject>
- (void)presentMatchesViewController;
@end

@interface MUMatchViewController : UIViewController
// Delegate property.
@property (weak) id<MUMatchViewControllerDelegate> delegate;
// This will allow us to pass the photo that we're currently presenting in the feed to the
// match view controller.
@property (strong, nonatomic) UIImage *matchedUserImage;
@end