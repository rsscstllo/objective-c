//
//  MUSettingsViewController.m
//  MatchedUp
//
//  Created by Ross Castillo on 6/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "MUSettingsViewController.h"

@interface MUSettingsViewController ()
@property (strong, nonatomic) IBOutlet UISlider *ageSlider;
@property (strong, nonatomic) IBOutlet UISwitch *menSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *womenSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *singleSwitch;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) IBOutlet UIButton *editProfileButton;
@property (strong, nonatomic) IBOutlet UILabel *ageLabel;
@end

@implementation MUSettingsViewController

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  self.ageSlider.value = [[NSUserDefaults standardUserDefaults] integerForKey:kMUAgeMaxKey];
  self.menSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:kMUMenEnabledKey];
  self.womenSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:kMUWomenEnabledKey];
  self.singleSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:kMUSingleEnabledKey];
  [self.ageSlider addTarget:self action:@selector(valueChanged:)
           forControlEvents:UIControlEventValueChanged];
  [self.menSwitch addTarget:self action:@selector(valueChanged:)
           forControlEvents:UIControlEventValueChanged];
  [self.womenSwitch addTarget:self action:@selector(valueChanged:)
             forControlEvents:UIControlEventValueChanged];
  [self.singleSwitch addTarget:self action:@selector(valueChanged:)
              forControlEvents:UIControlEventValueChanged];
  
  // Set the age label's text to be the initial value of the slider - cast from float to int.
  self.ageLabel.text = [NSString stringWithFormat:@"%i", (int)self.ageSlider.value];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)logoutButtonPressed:(UIButton *)sender {
  [PFUser logOut];
  [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)editProfileButtonPressed:(UIButton *)sender {
  // ...
}

#pragma mark - Helper Methods

// The sender will allow us to figure out whether it was slider that changed or which specific
// switch change.
- (void)valueChanged:(id)sender {
  if (sender == self.ageSlider) {
    [[NSUserDefaults standardUserDefaults] setInteger:(int)self.ageSlider.value
                                               forKey:kMUAgeMaxKey];
    self.ageLabel.text = [NSString stringWithFormat:@"%i", (int)self.ageSlider.value];
  } else if (sender == self.menSwitch) {
    [[NSUserDefaults standardUserDefaults] setBool:self.menSwitch.isOn
                                            forKey:kMUMenEnabledKey];
  } else if (sender == self.womenSwitch) {
    [[NSUserDefaults standardUserDefaults] setBool:self.womenSwitch.isOn
                                            forKey:kMUWomenEnabledKey];
  } else if (sender == self.singleSwitch) {
    [[NSUserDefaults standardUserDefaults] setBool:self.singleSwitch.isOn
                                            forKey:kMUSingleEnabledKey];
  }
  [[NSUserDefaults standardUserDefaults] synchronize];  // Save to NSUserDefaults.
}

@end