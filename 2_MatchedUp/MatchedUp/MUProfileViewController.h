//
//  MUProfileViewController.h
//  MatchedUp
//
//  Created by Ross Castillo on 6/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MUProfileViewControllerDelegate<NSObject>
- (void)didPressLike;
- (void)didPressDislike;
@end

@interface MUProfileViewController : UIViewController
@property (weak, nonatomic) id<MUProfileViewControllerDelegate> delegate;
@property (strong, nonatomic) PFObject *photo;
@end