//
//  MULoginViewController.m
//  MatchedUp
//
//  Created by Ross Castillo on 6/11/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "MULoginViewController.h"
#import "MUConstants.h"

@interface MULoginViewController ()
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
// Query info and build a NSObject as we get info back for URL image.
@property (strong, nonatomic) NSMutableData *imageData;
@end

@implementation MULoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  self.activityIndicator.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
  if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
    [self updateUserInformation]; // Reflect any changes user has made on his/her facebook account.
    [self performSegueWithIdentifier:@"loginToHomeSegue" sender:self];
  }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

// User must use facebook in order to log in to this application.
- (IBAction)loginButtonPressed:(UIButton *)sender {
  self.activityIndicator.hidden = NO;
  [self.activityIndicator startAnimating];
  // Naming conventions from facebook developers doc.
  NSArray *permissionsArray = @[@"user_about_me", @"user_interests", @"user_relationships",
                                @"user_birthday", @"user_location", @"user_relationship_details"];
  // Delegates the fb sdk to authenticate the user and creates a PFUser object.
  [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
    // Stop the activity indicator no matter if the log-in was successful or not.
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = YES;
    if (!user) {  // If we don't get a user back
      if (!error) {  // and if there's no error
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Log In Error"
                                                            message:@"Facebook login was cancelled"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        [alertView show];
      } else {  // If we don't get a user back but there is an error.
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Log In Error"
                                                            message:[error description]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
        [alertView show];
      }
    } else {  // If we do get a user back, update his or her info and segue to the home page.
      [self updateUserInformation];
      [self performSegueWithIdentifier:@"loginToHomeSegue" sender:self];
    }
  }];
}

#pragma mark - Helper Methods

- (void)updateUserInformation {
  // Send a request to facebook.
  FBRequest *request = [FBRequest requestForMe];
  [request startWithCompletionHandler:^(FBRequestConnection *connection, id result,
                                        NSError *error) {
    if (!error) {
      NSDictionary *userDictionary = (NSDictionary *)result;
      NSString *facebookID = userDictionary[@"id"];  // Create a URL for user picture.
      NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
      // |initWithCapacity| helps the program allocate enough memory for efficiency.
      NSMutableDictionary *userProfile = [[NSMutableDictionary alloc] initWithCapacity:8];
      // Persist user data.
      if (userDictionary[@"name"])  // If there's indeed a value for the name key, then
        userProfile[kMUUserProfileNameKey] = userDictionary[@"name"];  // add it to user profile.
      if (userDictionary[@"first_name"])
        userProfile[kMUUserProfileFirstNameKey] = userDictionary[@"first_name"];
      if (userDictionary[@"location"][@"name"])  // Embedded dictionary.
        userProfile[kMUUserProfileLocationKey] = userDictionary[@"location"][@"name"];
      if (userDictionary[@"gender"])
        userProfile[kMUUserProfileGenderKey] = userDictionary[@"gender"];
      if (userDictionary[@"birthday"]) {
        userProfile[kMUUserProfileBirthdayKey] = userDictionary[@"birthday"];
        // Convert this birthday into a NSDate.
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterShortStyle];
        // Pass in the date from our user dictionary.
        NSDate *date = [formatter dateFromString:userDictionary[@"birthday"]];
        NSDate *now = [NSDate date];  // Calling the |date| method gives today's current date.
        NSTimeInterval seconds = [now timeIntervalSinceDate:date];
        int age = seconds / 31536000;  // Age of user in years.
        userProfile[kMUUserprofileAgeKey] = @(age);  // Converting |age| into an NSNumber.
      }
      if (userDictionary[@"interested_in"])
        userProfile[kMUUserProfileInterestedInKey] = userDictionary[@"interested_in"];
      if ([pictureURL absoluteString])  // Persist our URL
        userProfile[kMUUserProfilePictureURL] = [pictureURL absoluteString];
      if (userDictionary[@"relationship_status"])
        userProfile[kMUUserProfileRelationshipStatusKey] = userDictionary[@"relationship_status"];
      [[PFUser currentUser] setObject:userProfile forKey:kMUUserProfileKey];
      [[PFUser currentUser] saveInBackground];
      [self requestImage]; // This will call helper methods.
    } else  // If there is an error.
      NSLog(@"Error in facebook request %@", error);
  }];
}

// Creating a PFFile that stores information. This method accepts an UIImage and sends it up to
// parse to save it for us.
- (void)uploadPFFileToParse:(UIImage *)image {
  // Converting UIImage into a NSData object, which stores pictures in core data.
  NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
  if (!imageData) {  // If for some reason there is no image data, end this method.
    NSLog(@"imageData was not found");
    return;
  }
  // PFFile accepts NSData object and stores it in parse more efficiently than storing raw images.
  PFFile *photoFile = [PFFile fileWithData:imageData];
  [photoFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
    if (succeeded) {
      PFObject *photo = [PFObject objectWithClassName:kMUPhotoClassKey];
      // Set up a key value pair so that my photo knows which user it belongs to.
      [photo setObject:[PFUser currentUser] forKey:KMUPhotoUserKey];
      [photo setObject:photoFile forKey:kMUPhotoPictureKey];
      [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        // Photo saved successfully.
      }];
    }
  }];
}

// Save the image to parse
- (void)requestImage {
  PFQuery *query = [PFQuery queryWithClassName:kMUPhotoClassKey];
  [query whereKey:KMUPhotoUserKey equalTo:[PFUser currentUser]];
  [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
    if (number == 0) { // If the user doesn't have a photo saved yet, then make request for URL.
      PFUser *user = [PFUser currentUser];
      // If we had something stored in data, allocate a new object.
      self.imageData = [[NSMutableData alloc] init];
      // Key into dictionary to get our picture URL.
      NSURL *profilePictureURL = [NSURL URLWithString:user[kMUUserProfileKey]
                                  [kMUUserProfilePictureURL]];
      // Set up some settings.
      NSURLRequest *urlRequest = [NSURLRequest requestWithURL:profilePictureURL
                                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval:4.0f];
      NSURLConnection *urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest
                                                                       delegate:self];
      if (!urlConnection)
        NSLog(@"Failed to download picture");
    }
  }];
}

// As chunks of the image are recieved, data file will build.
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
  [self.imageData appendData:data];  // build data image object
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
  UIImage *profileImage = [UIImage imageWithData:self.imageData];
  [self uploadPFFileToParse:profileImage];  // Save image to parse.
}

@end