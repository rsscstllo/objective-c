//
//  MUAppDelegate.h
//  MatchedUp
//
//  Created by Ross Castillo on 6/11/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MUAppDelegate : UIResponder<UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@end