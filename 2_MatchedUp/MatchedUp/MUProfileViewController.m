//
//  MUProfileViewController.m
//  MatchedUp
//
//  Created by Ross Castillo on 6/18/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "MUProfileViewController.h"

@interface MUProfileViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UILabel *ageLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
//@property (strong, nonatomic) IBOutlet UILabel *tagLineLabel;
@property (strong, nonatomic) IBOutlet UITextView *aboutMeTextView;
@end

@implementation MUProfileViewController

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  PFFile *pictureFile = self.photo[kMUPhotoPictureKey];
  [pictureFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
    self.profilePictureImageView.image = [UIImage imageWithData:data];
  }];
  PFUser *user = self.photo[KMUPhotoUserKey];
  self.locationLabel.text = user[kMUUserProfileKey][kMUUserProfileLocationKey];
  self.ageLabel.text = [NSString stringWithFormat:@"%@",
                        user[kMUUserProfileKey][kMUUserprofileAgeKey]];
  if (user[kMUUserProfileKey][kMUUserProfileRelationshipStatusKey] == nil)
    self.statusLabel.text = @"Single";
  else
    self.statusLabel.text = user[kMUUserProfileKey][kMUUserProfileRelationshipStatusKey];
  //self.tagLineLabel.text = user[kMUUserTagLineKey];
  self.aboutMeTextView.text = user[kMUUserTagLineKey];
  
  self.view.backgroundColor = [UIColor colorWithRed:242/255.0
                                              green:242/255.0
                                               blue:242/255.0
                                              alpha:1.0];
  self.title = user[kMUUserProfileKey][kMUUserProfileFirstNameKey];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)likeButtonPressed:(UIButton *)sender {
  [self.delegate didPressLike];
}

- (IBAction)dislikeButtonPressed:(UIButton *)sender {
  [self.delegate didPressDislike];
}

@end