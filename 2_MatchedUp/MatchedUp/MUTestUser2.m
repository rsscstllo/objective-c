//
//  MUTestUser2.m
//  MatchedUp
//
//  Created by Ross Castillo on 6/21/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "MUTestUser2.h"

@implementation MUTestUser2

+ (void)saveTestUserToParse {
  // Create a new user
  PFUser *newUser = [PFUser user];
  newUser.username = @"user2";
  newUser.password = @"password2";
  [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
    if (!error) {
      NSDictionary *profile = @{@"age" : @39,
                                @"birthday"   : @"1/3/1975",
                                @"firstName"  : @"Andrew",
                                @"gender"     : @"male",
                                @"location"   : @"Gainesville, Florida",
                                @"name"       : @"Andrew Rinzler"
                                };
      [newUser setObject:profile forKey:@"profile"];
      [newUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        UIImage *profileImage = [UIImage imageNamed:@"rinzler.jpg"];
        // Create a data representation of this image
        NSData *imageData = UIImageJPEGRepresentation(profileImage, 0.8);
        // Create a PFFile from the NSData just created above
        PFFile *photoFile = [PFFile fileWithData:imageData];
        [photoFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
          if (succeeded) {  // If in fact our PFFile was uploaded
            PFObject *photo = [PFObject objectWithClassName:kMUPhotoClassKey];
            [photo setObject:newUser forKey:KMUPhotoUserKey];
            [photo setObject:photoFile forKey:kMUPhotoPictureKey];
            [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
              NSLog(@"photo saved successfully");
            }];
          }
        }];
      }];
    }
  }];
}

@end