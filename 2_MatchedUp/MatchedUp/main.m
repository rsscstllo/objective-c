//
//  main.m
//  MatchedUp
//
//  Created by Ross Castillo on 6/11/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MUAppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([MUAppDelegate class]));
  }
}
