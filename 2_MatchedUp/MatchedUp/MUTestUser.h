//
//  MUTestUser.h
//  MatchedUp
//
//  Created by Ross Castillo on 6/20/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MUTestUser : NSObject
+ (void)saveTestUserToParse;
@end