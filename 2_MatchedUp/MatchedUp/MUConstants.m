//
//  MUConstants.m
//  MatchedUp
//
//  Created by Ross Castillo on 6/14/14.
//  Copyright (c) 2014 Ross Castillo. All rights reserved.
//

#import "MUConstants.h"

@implementation MUConstants

#pragma mark - User Class
NSString *const kMUUserProfileKey                    = @"profile";
NSString *const kMUUserProfileNameKey                = @"name";
NSString *const kMUUserProfileFirstNameKey           = @"firstName";
NSString *const kMUUserProfileLocationKey            = @"location";
NSString *const kMUUserProfileGenderKey              = @"gender";
NSString *const kMUUserProfileBirthdayKey            = @"birthday";
NSString *const kMUUserProfileInterestedInKey        = @"interestedIn";
NSString *const kMUUserProfilePictureURL             = @"pictureURL";
NSString *const kMUUserProfileRelationshipStatusKey  = @"relationShipStatus";
NSString *const kMUUserprofileAgeKey                 = @"age";
NSString *const kMUUserTagLineKey                    = @"tagLine";

#pragma mark - Photo Class
NSString *const kMUPhotoClassKey                     = @"Photo";  // Upper-case 'P' for class
NSString *const KMUPhotoUserKey                      = @"user";
NSString *const kMUPhotoPictureKey                   = @"image";

#pragma mark - Activity Class
NSString *const kMUActivityClassKey                  = @"Activity"; // Upper-case 'A' for class
NSString *const kMUActivityTypeKey                   = @"type";
NSString *const kMUActivityFromUserKey               = @"fromUser";
NSString *const kMUActivityToUserKey                 = @"toUser";
NSString *const kMUActivityPhotoKey                  = @"photo";
NSString *const kMUActivityTypeLikeKey               = @"like";
NSString *const kMUactivityTypeDislikeKey            = @"dislike";

#pragma mark - Settings
NSString *const kMUMenEnabledKey                     = @"men";
NSString *const kMUWomenEnabledKey                   = @"women";
NSString *const kMUSingleEnabledKey                  = @"single";
NSString *const kMUAgeMaxKey                         = @"ageMax";

#pragma mark - ChatRoom
NSString *const kMUChatRoomClassKey                  = @"ChatRoom";
NSString *const kMUChatRoomUser1Key                  = @"user1";
NSString *const kMUChatRoomUser2Key                  = @"user2";

#pragma mark - Chat
NSString *const kMUChatClassKey                      = @"Chat";
NSString *const kMUChatChatroomKey                   = @"chatroom";
NSString *const kMUChatFromUserKey                   = @"fromUser";
NSString *const kMUChatToUserKey                     = @"toUser";
NSString *const kMUChatTextKey                       = @"text";

@end